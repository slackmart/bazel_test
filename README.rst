Bazel test
----------


Getting started
===============

::

    $ tree
    .
    ├── README.rst
    ├── WORKSPACE
    ├── lambda
    │   ├── BUILD
    │   └── mongo_to_s3_to_redshift.py
    ├── martin
    │   ├── BUILD
    │   └── main.py
    └── requirements.txt

    2 directories, 7 files

    $ bazel clean && bazel build //:mongo_to_s3_to_redshift
    Extracting Bazel installation...
    Starting local Bazel server and connecting to it...
    INFO: Starting clean (this may take a while). Consider using --async if the clean takes more than several minutes.
    INFO: Analyzed target //:mongo_to_s3_to_redshift (18 packages loaded, 163 targets configured).
    INFO: Found 1 target...
    Target //:mongo_to_s3_to_redshift up-to-date:
      bazel-bin/mongo_to_s3_to_redshift
    INFO: Elapsed time: 8.410s, Critical Path: 0.05s
    INFO: 0 processes.
    INFO: Build completed successfully, 5 total actions

    $ ./bazel-bin/mongo_to_s3_to_redshift
    hell <module 'pymongo' from '/home/slackmart/Desktop/bazel_test/bazel-bin/mongo_to_s3_to_redshift.runfiles/lambda_deps_pypi__pymongo_3_10_1/pymongo/__init__.py'>

Requirements will be automatically installed in ./bazel-bin/mongo_to_s3_to_redshift.runfiles/

Now an example with CLI arguments

::

    $ bazel clean && bazel build //:say_hello
    INFO: Starting clean (this may take a while). Consider using --async if the clean takes more than several minutes.
    INFO: Analyzed target //:say_hello (18 packages loaded, 104 targets configured).
    INFO: Found 1 target...
    Target //:say_hello up-to-date:
      bazel-bin/say_hello
    INFO: Elapsed time: 4.287s, Critical Path: 0.03s
    INFO: 0 processes.
    INFO: Build completed successfully, 5 total actions

    $ bazel-bin/say_hello
    Your name: slackmart
    Hello slackmart!

    $ bazel-bin/say_hello --name slackmart
    Hello slackmart!
